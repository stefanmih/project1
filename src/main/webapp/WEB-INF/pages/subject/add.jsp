<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
<style type="text/css">
body{
padding:20px;
margin:20px;
}
#container{
box-sizing:content-box;
width: 300px;
height: 600px;
}
</style>
</head>
<body>
<div class="form-group" id="container">
<form:form action="/project1/subject/add" method="post" modelAttribute="SubjectDto">
<form:input class="form-control" type="hidden" path="id" value='<%=request.getAttribute("oldId") %>'/><br/>
<form:input class="form-control" type="text" path="name"/><br/>
<form:input class="form-control" type="text" path="description"/><br/>
<form:input class="form-control" type="text" path="yearOfStudy"/><br/>
<form:input class="form-control" type="text" path="semester"/><br/>
<button class="btn btn-primary" name="save">Save</button>
</form:form>
</div>
</body>
</html>