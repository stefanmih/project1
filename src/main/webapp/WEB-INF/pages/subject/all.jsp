<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style type="text/css">
	body{
	margin:20px;
	padding:20px;
	}</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table class="table">
<thead>
    <tr>
      <th scope="col">Ime</th>
      <th scope="col">Opis</th>
      <th scope="col">Godina</th>
      <th scope="col">Semestar</th>
      <th scope="col"></th>
				<th scope="col"></th>
    </tr>
  </thead>
<c:forEach items="${subjectAll}" var="item">
<tr>
<td>${item.name}</td>
<td>${item.description}</td>
<td>${item.yearOfStudy}</td>
<td>${item.semester}</td>
<td><form action="/project1/subject/edit" method="post">
						<input type="hidden" name="idEdit" value="${item.id}" /><input
							class="btn btn-primary" type="submit" value="Izmeni" />
					</form></td>
				<td><form action="/project1/subject/delete" method="post">
						<input type="hidden" name="idRemove" value="${item.id}" /><input
							class="btn btn-primary" type="submit" value="Obrisi" />
					</form></td>
</tr>
</c:forEach>
</table>
</body>
</html>