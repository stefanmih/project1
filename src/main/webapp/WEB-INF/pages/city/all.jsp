<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table class="table">
<thead>
    <tr>
      <th scope="col">Ptt Broj</th>
      <th scope="col">Naziv</th>
    </tr>
  </thead>
<c:forEach items="${cityAll}" var="item">
<tr>
<td>${item.pttBr}</td><td>${item.name}</td>
</tr>
</c:forEach>
</table>
</body>
</html>