<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<style type="text/css">
	body{
	margin:20px;
	padding:20px;
	}</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table class="table">
<thead>
    <tr>
      <th scope="col">Indeks</th>
      <th scope="col">Ime</th>
      <th scope="col">Prezime</th>
      <th scope="col">E-mail</th>
      <th scope="col">Adresa</th>
      <th scope="col">Grad</th>
      <th scope="col">Telefon</th>
      <th scope="col">Godina studiranja</th>
      <th scope="col"></th>
				<th scope="col"></th>
    </tr>
  </thead>
<c:forEach items="${studentAll}" var="item">
<tr>
<td>${item.indexNumber}</td>
<td>${item.firstName}</td>
<td>${item.lastName}</td>
<td>${item.email}</td>
<td>${item.address}</td>
<td>${item.city.pttBr} , ${item.city.name}</td>
<td>${item.phone}</td>
<td>${item.currentYearOfStudy}</td>
<td><form action="/project1/student/edit" method="post">
						<input type="hidden" name="idEdit" value="${item.indexNumber}" /><input
							class="btn btn-primary" type="submit" value="Izmeni" />
					</form></td>
				<td><form action="/project1/student/delete" method="post">
						<input type="hidden" name="idRemove" value="${item.indexNumber}" /><input
							class="btn btn-primary" type="submit" value="Obrisi" />
					</form></td>
</tr>
</c:forEach>
</table>
</body>
</html>