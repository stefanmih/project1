<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
body{
padding:20px;
margin:20px;
}
#container{
box-sizing:content-box;
width: 300px;
height: 600px;
}
</style>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
</head>
<body><div class="form-group" id="container">

<form:form action="/project1/professor/add" method="post" modelAttribute="ProfessorDto">
<form:input path="id" readonly="true" type="hidden" value='<%=request.getAttribute("oldId") %>'/>
<form:input class="form-control" type="text" path="firstName"/><br/>
<form:input class="form-control" type="text" path="lastName"/><br/>
<form:input class="form-control" type="text" path="email"/><br/>
<form:input class="form-control" type="text" path="address"/><br/>
<form:select class="form-control" path="city.pttBr"><br/>
<c:forEach items="${cityAll}" var="item">
<form:option value="${item.pttBr}">${item.pttBr}, ${item.name}</form:option>
</c:forEach>
</form:select><br/>
<form:input class="form-control" type="text" path="phone"/><br/>
<form:input class="form-control" type="date" path="reelectionDate"/><br/>
<form:select class="form-control" path="title.id">
<c:forEach items="${titleAll}" var="item">
<form:option value="${item.id}">${item.title}</form:option>
</c:forEach>
</form:select><br/>
<button class="btn btn-primary" name="save">Sacuvaj</button>
</form:form>
</div>
</body>
</html>