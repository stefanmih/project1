package stefan.mihajlovic.fon.project1.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "subject")
@NamedQuery(name="SubjectEntity.findAll", query="SELECT s FROM SubjectEntity s")
public class SubjectEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String description;
	private Long yearOfStudy;
	private String semester;
	public SubjectEntity(Long id, String name, String description, Long yearOfStudy, String semester) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	}
	public SubjectEntity() {
		super();
	}
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getYearOfStudy() {
		return yearOfStudy;
	}
	public void setYearOfStudy(Long yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	@Override
	public String toString() {
		return "SubjectEntity [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy=" + yearOfStudy
				+ ", semester=" + semester + "]";
	}
	
}
