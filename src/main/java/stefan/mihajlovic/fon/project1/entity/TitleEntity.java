package stefan.mihajlovic.fon.project1.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "title")
@NamedQuery(name="TitleEntity.findAll", query="SELECT t FROM TitleEntity t")
public class TitleEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String title;
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = false)
	private List<ProfessorEntity> professors = new ArrayList<>();

	public TitleEntity(Long id,String title) {
		super();
		this.id=id;
		this.title = title;
	}

	public TitleEntity() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	

	public List<ProfessorEntity> getProfessors() {
		return professors;
	}

	public void setProfessors(List<ProfessorEntity> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return "TitleEntity [title=" + title + "]";
	}

}
