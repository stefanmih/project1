package stefan.mihajlovic.fon.project1.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "city")
@NamedQuery(name="CityEntity.findAll", query="SELECT c FROM CityEntity c")
public class CityEntity {
	@Id
	Long pttBr;
	String name;
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = false)
	private List<ProfessorEntity> professors = new ArrayList<>();
	
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, orphanRemoval = false)
	private List<StudentEntity> students = new ArrayList<>();
	
	public CityEntity(Long pttBr, String name) {
		super();
		this.pttBr = pttBr;
		this.name = name;
	}
	
	public List<ProfessorEntity> getProfessors() {
		return professors;
	}

	public void setProfessors(List<ProfessorEntity> professors) {
		this.professors = professors;
	}

	public List<StudentEntity> getStudents() {
		return students;
	}

	public void setStudents(List<StudentEntity> students) {
		this.students = students;
	}

	public CityEntity() {
		super();
	}
	public Long getPttBr() {
		return pttBr;
	}
	public void setPttBr(Long pttBr) {
		this.pttBr = pttBr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "CityEntity [pttBr=" + pttBr + ", name=" + name + "]";
	}
	
	
	
}
