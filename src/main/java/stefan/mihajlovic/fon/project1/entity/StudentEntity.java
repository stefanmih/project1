
package stefan.mihajlovic.fon.project1.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "student")
@NamedQuery(name="StudentEntity.findAll", query="SELECT s FROM StudentEntity s")
public class StudentEntity {
	@Id
	private String indexNumber;
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	@ManyToOne(cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REMOVE })
	private CityEntity city;
	private String phone;
	private Long currentYearOfStudy;
	public StudentEntity(String indexNumber, String firstName, String lastName, String email, String address, CityEntity city,
			String phone, Long currentYearOfStudy) {
		super();
		this.indexNumber = indexNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}
	
	public StudentEntity() {
	}
	public String getIndexNumber() {
		return indexNumber;
	}
	
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public CityEntity getCity() {
		return city;
	}
	public void setCity(CityEntity city) {
		this.city = city;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Long getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}
	public void setCurrentYearOfStudy(Long currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}
	@Override
	public String toString() {
		return "StudentEntity [indexNumber=" + indexNumber + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", email=" + email + ", address=" + address + ", city=" + city + ", phone=" + phone
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	
	
}
