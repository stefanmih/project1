package stefan.mihajlovic.fon.project1.dto;

public class TitleDto {
	Long id;
	String name;
	public TitleDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public TitleDto() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "TitleDto [id=" + id + ", name=" + name + "]";
	}
	
	
}
