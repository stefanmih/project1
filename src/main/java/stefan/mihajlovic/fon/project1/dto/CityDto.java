package stefan.mihajlovic.fon.project1.dto;

import javax.validation.constraints.NotNull;

public class CityDto {
	@NotNull(message = "Morete uneti ptt broj")
	Long pttBr;
	String name;
	public CityDto(Long pttBr, String name) {
		super();
		this.pttBr = pttBr;
		this.name = name;
	}
	public CityDto() {
		super();
	}
	public Long getPttBr() {
		return pttBr;
	}
	public void setPttBr(Long pttBr) {
		this.pttBr = pttBr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "CityDto [pttBr=" + pttBr + ", name=" + name + "]";
	}
	
	
	
}
