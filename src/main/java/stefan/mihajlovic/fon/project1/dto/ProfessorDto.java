package stefan.mihajlovic.fon.project1.dto;

import java.util.Date;

import javax.validation.constraints.NotEmpty;

import org.springframework.format.annotation.DateTimeFormat;

public class ProfessorDto {
	private Long id;
	private String firstName, lastName;
	@NotEmpty(message = "Ne sme biti prazno")
	private String email;
	private String address;
	private CityDto city;
	private String phone;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date reelectionDate;
	private TitleDto title;

	public ProfessorDto(Long id, String firstName, String lastName, String email, String address, CityDto city,
			String phone, Date reelectionDate, TitleDto title) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.city = city;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ProfessorDto() {
		super();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getlastName() {
		return lastName;
	}

	public void setlastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CityDto getCity() {
		return city;
	}

	public void setCity(CityDto city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getReelectionDate() {
		return reelectionDate;
	}

	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}

	public TitleDto getTitle() {
		return title;
	}

	public void setTitle(TitleDto title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "ProfessorDto [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", address="
				+ address + ", city=" + city + ", phone=" + phone + ", reelectionDate=" + reelectionDate + ", title="
				+ title + "]";
	}

}
