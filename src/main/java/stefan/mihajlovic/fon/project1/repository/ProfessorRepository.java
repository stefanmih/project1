package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.ProfessorEntity;

@Repository(value = "professorJpaRepository")
@Transactional
@Service
public class ProfessorRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public void add(ProfessorEntity c) {
		entityManager.merge(c);
	}
	@SuppressWarnings("unchecked")
	public List<ProfessorEntity> getAll() {
		return entityManager.createNamedQuery("ProfessorEntity.findAll").getResultList();
	}
	public ProfessorEntity findById(Long id) {
		return entityManager.find(ProfessorEntity.class, id);
	} 
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
