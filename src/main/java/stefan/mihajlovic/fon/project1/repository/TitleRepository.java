package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.TitleEntity;

@Repository(value = "titleJpaRepository")
@Transactional
@Service
public class TitleRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public void add(TitleEntity c) {
		entityManager.merge(c);
	}
	@SuppressWarnings("unchecked")
	public List<TitleEntity> getAll() {
		return entityManager.createNamedQuery("TitleEntity.findAll").getResultList();
	}
	public TitleEntity findById(Long id) {
		return entityManager.find(TitleEntity.class, id);
	} 
}