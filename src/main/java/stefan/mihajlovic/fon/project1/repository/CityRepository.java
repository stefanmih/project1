package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.CityEntity;

@Repository(value = "cityJpaRepository")
@Transactional
@Service
public class CityRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public void add(CityEntity c) {
		entityManager.persist(c);
	}
	@SuppressWarnings("unchecked")
	public List<CityEntity> getAll() {
		return entityManager.createNamedQuery("CityEntity.findAll").getResultList();
	}
	public CityEntity findById(Long id) {
		return entityManager.find(CityEntity.class, id);
	}
	
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
