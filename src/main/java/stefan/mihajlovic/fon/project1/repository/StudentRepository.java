package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.StudentEntity;

@Repository(value = "studentJpaRepository")
@Transactional
@Service
public class StudentRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public void add(StudentEntity c) {
		entityManager.merge(c);
	}
	@SuppressWarnings("unchecked")
	public List<StudentEntity> getAll() {
		return entityManager.createNamedQuery("StudentEntity.findAll").getResultList();
	}
	public StudentEntity findById(String id) {
		return entityManager.find(StudentEntity.class, id);
	}
	public void remove(String id) {
		entityManager.remove(findById(id));
		
	} 
}
