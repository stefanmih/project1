package stefan.mihajlovic.fon.project1.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import stefan.mihajlovic.fon.project1.entity.SubjectEntity;

@Repository(value = "subjectJpaRepository")
@Transactional
@Service
public class SubjectRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public void add(SubjectEntity c) {
		entityManager.merge(c);
	}
	@SuppressWarnings("unchecked")
	public List<SubjectEntity> getAll() {
		return entityManager.createNamedQuery("SubjectEntity.findAll").getResultList();
	}
	public SubjectEntity findById(Long id) {
		return entityManager.find(SubjectEntity.class, id);
	}
	
	public void remove(Long id) {
		entityManager.remove(findById(id));
	}
}
