package stefan.mihajlovic.fon.project1.domain;

public class Title {
	Long id;
	String name;
	public Title(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Title() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Title [id=" + id + ", name=" + name + "]";
	}
	
}
