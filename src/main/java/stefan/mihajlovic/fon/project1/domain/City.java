package stefan.mihajlovic.fon.project1.domain;

public class City {
	Long pttBr;
	String name;
	public City(Long pttBr, String name) {
		super();
		this.pttBr = pttBr;
		this.name = name;
	}
	public City() {
		super();
	}
	public Long getPttBr() {
		return pttBr;
	}
	public void setPttBr(Long pttBr) {
		this.pttBr = pttBr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "City [pttBr=" + pttBr + ", name=" + name + "]";
	}
	
	
	
}
