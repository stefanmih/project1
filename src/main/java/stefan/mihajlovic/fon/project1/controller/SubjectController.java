
package stefan.mihajlovic.fon.project1.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.SubjectDto;
import stefan.mihajlovic.fon.project1.entity.SubjectEntity;
import stefan.mihajlovic.fon.project1.repository.SubjectRepository;

@Controller
@RequestMapping(value = "/subject", method = { RequestMethod.GET, RequestMethod.POST })
public class SubjectController {
	@Autowired
	SubjectRepository subjectRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("subject/add");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		return modelAndView;
	}
	
	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("subject/find");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		return modelAndView;
	}

	@PostMapping(value = "add")
	public String add(
			@ModelAttribute(name="SubjectDto")SubjectDto subjectDto, 
			@RequestParam(name = "save") String action,
			SessionStatus sessionStatus) throws Exception {
		SubjectEntity subjectEntity = new SubjectEntity();
		subjectEntity.setDescription(subjectDto.getDescription());
		subjectEntity.setId(subjectDto.getId());
		subjectEntity.setName(subjectDto.getName());
		subjectEntity.setSemester(subjectDto.getSemester());
		subjectEntity.setYearOfStudy(subjectDto.getYearOfStudy());
		subjectRepository.add(subjectEntity);
		return "redirect:/subject/all";
	}

	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("subject/all");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		if (request.getParameter("id") != null) {
			request.setAttribute("subjectAll", Arrays.asList(subjectRepository.findById(Long.parseLong(request.getParameter("id")))));
		} else {
			request.setAttribute("subjectAll", subjectRepository.getAll());
		}
		return modelAndView;
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request,HttpServletResponse response){
		String attr=request.getParameter("idEdit");
		ModelAndView modelAndView=new ModelAndView("subject/add");
		modelAndView.addObject("SubjectDto",new SubjectDto());
		request.setAttribute("oldId", attr);
		request.setAttribute("cityAll", subjectRepository.getAll());
		return modelAndView;
	}
	
	@PostMapping(value = "delete")
	public ModelAndView remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("subject/remove");
		modelAndView.addObject("SubjectDto", new SubjectDto());
		req.setAttribute("message", "Obrisan Predmet: "+subjectRepository.findById(Long.parseLong(req.getParameter("idRemove"))));
		subjectRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return modelAndView;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView error(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView=new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}
}
