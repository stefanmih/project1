package stefan.mihajlovic.fon.project1.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/")
public class LoginController {
	@GetMapping
	public ModelAndView login(HttpServletRequest req,HttpServletResponse resp) {
		ModelAndView modelAndView=new ModelAndView("authentication/login");
		return modelAndView;
	}
	@PostMapping
	public ModelAndView signIn(HttpServletRequest req,HttpServletResponse resp) {
		ModelAndView modelAndView=new ModelAndView("authentication/login");
		return modelAndView;
	}
}
