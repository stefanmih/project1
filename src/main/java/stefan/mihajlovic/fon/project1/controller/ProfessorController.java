package stefan.mihajlovic.fon.project1.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.ProfessorDto;
import stefan.mihajlovic.fon.project1.entity.ProfessorEntity;
import stefan.mihajlovic.fon.project1.repository.CityRepository;
import stefan.mihajlovic.fon.project1.repository.ProfessorRepository;
import stefan.mihajlovic.fon.project1.repository.TitleRepository;

@Controller
@RequestMapping(value = "/professor", method = { RequestMethod.GET, RequestMethod.POST })
public class ProfessorController {
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	ProfessorRepository professorRepository;
	
	@Autowired
	TitleRepository titleRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("professor home");
		req.setAttribute("cityAll", cityRepository.getAll());
		req.setAttribute("titleAll", titleRepository.getAll());
		ModelAndView modelAndView = new ModelAndView("professor/add");
		modelAndView.addObject("ProfessorDto", new ProfessorDto());
		return modelAndView;
	}

	@PostMapping(value = "add")
	public String add(
			@ModelAttribute(name="ProfessorDto")ProfessorDto professorDto, 
			@RequestParam(name = "save") String action,
			SessionStatus sessionStatus) throws Exception {
		
		System.out.println(professorDto);
		ProfessorEntity professorEntity=new ProfessorEntity();
		professorEntity.setId(professorDto.getId());
		professorEntity.setAddress(professorDto.getAddress());
		professorEntity.setCity(cityRepository.findById(professorDto.getCity().getPttBr()));
		professorEntity.setEmail(professorDto.getEmail());
		professorEntity.setFirstName(professorDto.getFirstName());
		professorEntity.setLastName(professorDto.getlastName());
		professorEntity.setPhone(professorDto.getPhone());
		professorEntity.setReelectionDate(professorDto.getReelectionDate());
		professorEntity.setTitle(titleRepository.findById(professorDto.getTitle().getId()));
		professorRepository.add(professorEntity);
		
		return "redirect:/professor/all";
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request,HttpServletResponse response){
		String attr=request.getParameter("idEdit");
		
		ModelAndView modelAndView=new ModelAndView("professor/add");
		modelAndView.addObject("ProfessorDto",new ProfessorDto());
		request.setAttribute("oldId", attr);
		request.setAttribute("cityAll", cityRepository.getAll());
		request.setAttribute("titleAll", titleRepository.getAll());
		return modelAndView;
	}
	
	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("professor/all");
		modelAndView.addObject("ProfessorDto", new ProfessorDto());
		if (request.getParameter("id") != null) {
			request.setAttribute("professorAll", Arrays.asList(professorRepository.findById(Long.parseLong(request.getParameter("id")))));
		} else {
			request.setAttribute("professorAll", professorRepository.getAll());
		}
		return modelAndView;
	}
	
	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("professor/find");
		modelAndView.addObject("professorDto", new ProfessorDto());
		return modelAndView;
	}
	
	@PostMapping(value = "delete")
	public ModelAndView remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("professor/remove");
		modelAndView.addObject("professorDto", new ProfessorDto());
		req.setAttribute("message", "Obrisan profesor: "+professorRepository.findById(Long.parseLong(req.getParameter("idRemove"))));
		professorRepository.remove(Long.parseLong(req.getParameter("idRemove")));
		return modelAndView;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView error(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView=new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}
}
