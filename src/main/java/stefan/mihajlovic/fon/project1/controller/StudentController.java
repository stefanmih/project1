package stefan.mihajlovic.fon.project1.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import stefan.mihajlovic.fon.project1.dto.StudentDto;
import stefan.mihajlovic.fon.project1.entity.StudentEntity;
import stefan.mihajlovic.fon.project1.repository.CityRepository;
import stefan.mihajlovic.fon.project1.repository.StudentRepository;

@Controller
@RequestMapping(value = "/student", method = { RequestMethod.GET, RequestMethod.POST })
public class StudentController {
	@Autowired
	CityRepository cityRepository;
	
//	@Autowired
//	ProfessorRepository professorRepository;
	
	@Autowired
	StudentRepository studentRepository;

	@PostMapping
	public ModelAndView home(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("student home");
		req.setAttribute("cityAll", cityRepository.getAll());
		ModelAndView modelAndView = new ModelAndView("student/add");
		modelAndView.addObject("StudentDto", new StudentDto());
		return modelAndView;
	}

	@PostMapping(value = "add")
	public String add(
			@ModelAttribute(name="StudentDto")StudentDto studentDto, 
			@RequestParam(name = "save") String action,
			SessionStatus sessionStatus) throws Exception {
		StudentEntity StudentEntity = new StudentEntity();
		StudentEntity.setAddress(studentDto.getAddress());
		StudentEntity.setCity(cityRepository.findById(studentDto.getCity().getPttBr()));
		StudentEntity.setEmail(studentDto.getEmail());
		StudentEntity.setFirstName(studentDto.getFirstName());
		StudentEntity.setLastName(studentDto.getLastName());
		StudentEntity.setPhone(studentDto.getPhone());
		StudentEntity.setIndexNumber(studentDto.getIndexNumber());
		StudentEntity.setCurrentYearOfStudy(studentDto.getCurrentYearOfStudy());
		studentRepository.add(StudentEntity);
		return "redirect:/student/all";
	}
	
	@PostMapping(value = "all")
	public ModelAndView all(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView modelAndView = new ModelAndView("student/all");
		modelAndView.addObject("StudentDto", new StudentDto());
		if (request.getParameter("id") != null) {
			request.setAttribute("studentAll", Arrays.asList(studentRepository.findById(request.getParameter("id"))));
		} else {
			request.setAttribute("studentAll", studentRepository.getAll());
		}
		return modelAndView;
	}
	
	@PostMapping(value = "find")
	public ModelAndView find(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("student/find");
		modelAndView.addObject("studentDto", new StudentDto());
		return modelAndView;
	}
	
	@PostMapping(value = "edit")
	public ModelAndView edit(HttpServletRequest request,HttpServletResponse response){
		String attr=request.getParameter("idEdit");
		
		ModelAndView modelAndView=new ModelAndView("student/add");
		modelAndView.addObject("StudentDto",new StudentDto());
		request.setAttribute("oldId", attr);
		request.setAttribute("readonly", true);
		request.setAttribute("cityAll", cityRepository.getAll());
		return modelAndView;
	}
	
	@PostMapping(value = "delete")
	public ModelAndView remove(HttpServletRequest req, HttpServletResponse resp) {
		System.out.println("radi");
		ModelAndView modelAndView = new ModelAndView("student/remove");
		modelAndView.addObject("StudentDto", new StudentDto());
		req.setAttribute("message", "Obrisan Student: "+studentRepository.findById(req.getParameter("idRemove")));
		studentRepository.remove(req.getParameter("idRemove"));
		return modelAndView;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView error(HttpServletRequest request, HttpServletResponse response, Exception e) {
		ModelAndView modelAndView=new ModelAndView("error/error");
		request.setAttribute("error", e.getMessage());
		return modelAndView;
	}
}
